<?php

session_start();

require_once './vendor/autoload.php';
require_once './tools/helpers.php';
require_once './config/settings.php';
// aff($_SESSION);
require_once 'controllers/dateController.php';

$loader = new \Twig\Loader\FilesystemLoader('./views');
$twig   = new \Twig\Environment($loader, [
	'cache' => APP['twigCache'],
	'debug' => APP['twigDebug'],
]);
$twig->addGlobal('session', $_SESSION);
$twig->addExtension(new \Twig\Extension\DebugExtension());

if ($_SESSION['user'] ?? null) {
	switch (getUri()) {
		case '/':
			$page = 'month.html';

			break;
		case '/an':
			$page = 'year';

			break;
			
		case '/week':
			$page = 'week.html';

			break;

		case '/month':
			$page = 'month.html';

			break;
		case '/year':
			$page = 'year.html';

			break;
			
			
		case '/cal':
			$page = 'calendar';

			break;
		case '/events':
			$page = 'homeIni';

			break;
		case '/mob':
			$page = 'monthCalendar';

			break;
		case '/compte':
			$page = 'forms';

			break;
		case '/users':
			$page = 'users';

			break;
		case '/migration':
			$page = 'migration';

			break;
		case '/t':
			$page = 'uuutest';

			break;
			
			
			
		case '/cal':
			$page = 'calendar';

			break;
		case '/hini':
			$page = 'homeIni';
	
			break;
				
			
				
		default:
			$page = 'error';
	}
} else {
	$page = 'forms';
}

require_once controllers($page);
$template = $twig->load('pages/' . $page . 'View.twig');

echo $template->render(
	[
		'title' => $title ?? null,
		'data'  => $data ?? null,
	]
);