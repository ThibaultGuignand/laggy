<?php

/**
 * Request.
 */
function req(string $sql, array $args = [])
{
	include __DIR__ . '/cnx.php';

	try {
		$req = $connection->prepare($sql);
		foreach ($args as $k => $v) {
			// $argsCleaned[] = filter_var($v, FILTER_SANITIZE_SPECIAL_CHARS);
			$req->bindValue(':v', $v, PDO::PARAM_INT);
			aff($req);
		}
		$req->execute($argsCleaned ?? []);
	} catch (Exception $e) {
		echo 'Erreur : ' . $e->getMessage() . '<br />';
	}
// aff($sql);exit;
	return $req->fetchAll(PDO::FETCH_ASSOC);
}

// 	//On demande au SGBD de préparer (de précompiler) la requête
// 	//Le " ? " indique qu'il y aura un paramètre à cet endroit
// 	$smt = $db->prepare ( "SELECT * FROM livres WHERE isbn = ?");
	
// 	//On renseigne les paramètres 
// 	$stmt->bindParam(1,$_GET['isbn']) ;

// 	//On exécute la requête
// 	$stmt->execute() ;