<?php
if ( ($_GET['action'] ?? null) == 'deconnection'){
	aff('Nett session');
	session_destroy();
}
aff($_SESSION ?? null, 'SESSION');

require_once './tools/database/req.php';
require_once './models/userModel.php';

$title = 'Compte';

function isValidPseudo($username)
{
	if (preg_match('/^[a-zA-Z0-9\-\.\s]{2,255}$/', $username)) {
		return 1;
	}

	return 0;
}

function isValidPassword1($password, $password2)
{
	if (preg_match('/^[a-zA-Z0-9\-\.\s]{3,255}$/', $password) && ($password === $password2)) {
		return 1;
	}

	return 0;
}

$_POST['action'] ??= null;

if ("S'enregistrer" == $_POST['action']) {
	aff($_POST, 'POST');

	$pseudo    = ($_POST['username2']) ?? '';
	$email     = ($_POST['email']) ?? '';
	$password  = ($_POST['pass']) ?? '';
	$password2 = ($_POST['pass2']) ?? '';

	function isValidEmail($email)
	{
		if (preg_match('/^[^@\s]+@[^@\s\.][^@\s]+$/', $email)) {
			return 1;
		}

		return 0;
	}

	function isValidRegister($pseudo, $email, $password, $password2)
	{
		return isValidPseudo($pseudo) && isValidEmail($email) && isValidPassword1($password, $password2);
	}

	$controleData    = isValidRegister($pseudo, $email, $password, $password2);
	$data['message'] = $controleData ? 'L\'inscription est valide.' : 'L\'inscription est incorrecte.';
	if ($controleData) {
		$sql = 'INSERT INTO users (name, email, password) VALUES (\'' . $pseudo . '\', \'' . $email . '\', \'' . $password . '\')';
		afxf($sql);
		req($sql);
	}
} elseif ('Se connecter' == $_POST['action']) {
	$pseudo2 = ($_POST['username']) ?? '';
	$pass3   = ($_POST['password']) ?? '';

	// Si isValid le pseudo et le mot de passe on vérifie que le password indiqué est bien celui enregistré dans la BdD

	$data['access'] = (int) getIdIfUserExists($pseudo2, $pass3);
	if ($data['access']) {
		$_SESSION['user'] = [
			'id'   => $data['access'],
			'name' => $data['user'] = $pseudo2
			];
		$_SESSION['view'] = [
			'name'   => 'month'
			];
		aff($_SESSION);
		header('Location: ' . '/');
		exit;
	}
}