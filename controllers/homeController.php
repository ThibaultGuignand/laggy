<?php

use Carbon\Carbon;

require_once './controllers/dateController.php';

// session_destroy();
// $_SESSION['d'] = serialize((new Carbon('2022-08-7'))->locale('fr'));

$dt = getDt();
// aff($dt->day . '/' . $dt->month, 'DT');

$data['columnsHeads'] = getColumnHeads();

if (!empty($_GET['mois'])) {
	switch ($_GET['mois']) {
		case 'avant':
			$dt->subMonths();

			break;
		case 'courant':
			$dt = Carbon::now();

			break;
		case 'suivant':
			$dt->addMonths();

			break;
		default:
			header('Location: ' . '/');
			exit;

			break;
	}
	$_GET['depl'] = null;
}

$data['month'] = getCalendarVars($dt);

$_SESSION['d'] = serialize($dt);