<?php

use Carbon\Carbon;

require_once './controllers/dateController.php';

// session_destroy();
// $_SESSION['d'] = serialize((new Carbon('2022-08-7'))->locale('fr'));

$now           = (new Carbon())->locale('fr');
$_SESSION['d'] = $_SESSION['d'] ?? null ?: serialize($now);
$dt            = unserialize($_SESSION['d']);
// aff($dt->day . '/' . $dt->month, 'DT');

if (!empty($_GET)) {
	switch ($_GET['depl']) {
		case 'previous':
			$dt->subMonths();

			break;
		case 'next':
			$dt->addMonths();

			break;
		default:
			header('Location: ' . '/error');
			exit;

			break;
	}
	$_GET['depl'] = null;
}

$data['d']   = $dt->day;
$data['m']   = $dt->month;
$data['mFr'] = $dt->monthName;
$data['y']   = $dt->year;

$data['day0']              = $dt->copy()->firstOfMonth()->dayOfWeekIso;
$data['days']              = $dt->daysInMonth;
$data['daysPreviousMonth'] = $dt->copy()->subMonth()->daysInMonth;

$_SESSION['d'] = serialize($dt);